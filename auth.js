'use strict';

const uuid = require('uuid');
const crypto = require('crypto');

const secret = 'sonoSuperSegreto';

function authManager(ops)
{
    let db = ops.db;
    
    let _collection = db.collection("account")
    
    const results =
    {
        signIn,
        signUp,
        validateToken,
        forgotPassword,
        getByEmail,
        getById
        
    }
    
    return results;
    
    
    function signIn(ops, callback)
    {
        
        _findOne(_collection, {username:ops.username},(err,doc) =>
        {
            if (err)
                callback(err,null)
            else 
            {
                let res = 
                {
                    founded :true
                }
                if (!doc || doc.password !== ops.password)
                {
                    res.founded = false
                } 
                else
                {
                    res.token = _generateToken(doc),
                    res.id = doc._id    
                }
                
                callback(null,res); 
            }
        })

        
           
    }
    
    function signUp(ops, callback)
    {
       getByEmail(
            {
                email:ops.username
            },
            (err,res) => {
               if (err) 
               {
                  callback(err)  
               }
               else 
               {
                   if (res && res._id)
                   {
                     callback(null, {
                         created : false
                     })   
                   } 
                   else 
                   {
                       
                       let newUser = {
                           _id: uuid.v4(),
                           username: ops.username,
                           password: ops.password,
                           firtName: ops.firstName,
                           surname: ops.surname,
                       };
                       
                       _insertDoc(_collection,
                           newUser, (err,doc)=>
                           {
                               if (doc)
                                   doc.created = true
                               callback(err,doc);  
                           });
                   }
               }
            }
       );
    }
   
    function getById(ops,callback)
    {
       _findOne(_collection,{_id:ops._id}, (err,doc)=>
       {
           if (err)
           {
               callback(err)
           } 
           else 
           {
               //console.log("User", doc)
               callback(err,doc)
           }
       });
        
     
    }
    
    function getByEmail(ops,callback)
    {
        _findOne(_collection,{username:ops.email}, (err,doc)=>
       {
           if (err)
           {
               callback(err)
           } 
           else 
           {
               callback(err,doc)
           }
       });
    }
    
    function validateToken(ops, callback)
    {
        _parseToken(ops.token,(err,doc) =>
        {
            if (err)
            {
                callback(err)    
            } 
            else
            {
                if (!doc.valid)
                 callback(null, {
                    valid : doc.valid
                })
                else
                callback(null, {
                    valid : doc.valid,
                    token : ops.token,
                    id    : doc.user._id
                })
            }
        }) 
       
          
    }
    
    function forgotPassword(ops, callback)
    {
        const res = {};

        callback(null,res);    
    }
    
    
    function _insertDoc(coll, doc, callback)
    {
        coll.insertOne(doc,(err,res) =>
        {
            if (err)
                callback(err,res);
            else
                callback(null,res.ops[0])
        })
        
    }
    
    function _findOne(coll,query, callback)
    {
        coll.findOne(query,(err,doc) =>
        {
            if (err)
                callback(err,doc);
            else
                callback(null,doc)
        })
        
    }
    
    function _generateToken(doc)
    {
        const hash = crypto.createHmac('sha256', secret)
                        .update(doc.id+doc.username+doc.password)
                        .digest('hex');
       
        let b = doc._id + '.' +hash
        //console.log("TOKEN GENERATO",b)
        return b;
    }
    
    function _parseToken(token, callback)
    {
        var elements = token.split('.');
        
        if (elements && elements.length == 2)
        {
            getById({_id:elements[0]},(err,doc) =>
            {
               if (err)
                    callback(err) 
               else
               {
                  if(doc)
                  {
                     let newToken = _generateToken(doc);
                     let isValid = newToken.split('.')[1] == elements[1];
                     callback(null,
                     {
                         valid : isValid,
                         user : doc
                     })
                  }
                  else
                  {
                    callback(null, {valid:false});
                  }
               }     
            });
        } 
        else 
        {
            callback(null,
            {
                valid:false
            })
        }
        
 
    }
}

module.exports = authManager