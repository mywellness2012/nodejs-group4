
'use strict';

const mongoClient = require('mongodb').MongoClient;
const uuid = require('uuid');

const Code = require('code')// assertion library 
const Lab = require('lab')
const lab = exports.lab = Lab.script()

const expect = Code.expect;
const beforeEach = lab.beforeEach
const afterEach = lab.afterEach
const test = lab.test

const ops = {}
let authManager;

beforeEach( (done)=>{
   mongoClient.connect("mongodb://localhost/auth_db",(err,db) =>
   {
       if (err)
       {
            done(err)
       }
       else 
       {
            ops.db = db;
            db.collection('account').remove({},(err,res) =>
            {
                if (err)
                    done(err);
                else 
                {
                   _fillData(db, (err,doc) =>
                   { 
                       if (err)
                       {
                           done(err);
                       }
                       else 
                       {
                           authManager = require('./auth.js')(ops);
                           done();
                       }
                   });
                }
            })
       }
   } );
  
  
  
});

afterEach( (done) => 
{
    ops.db.close();
    done();
})

function _fillData(db,callback)
{
    db.collection('account').insertOne
    (
        {
            _id: uuid.v4(),
            username: 'unit@test.it',
            password: '123',
            firtName: 'test',
            surname: 'surname',   
         }, (err,results) =>
             {
                callback(err,results)
             }
    );
}

test('SignIn with username and password', (done) =>
{
   
    authManager.signIn
    (
        {
            username : 'unit@test.it',
            password: '123'
        },
        (err,result) => 
        {
            if (err){
                done(err)
            } else {
                expect(result.id).to.exist()
                expect(result.token).to.exist()
                expect(result.founded).to.equal(true)
                console.log(result.token)
                done()
            }    
        }
    ) 
             
})

test('signIn failed ', (done) =>
{
    authManager.signIn
    (
        {
            username : 'unit@test.it',
            password: '1234'
        },
        (err,result) => 
        {
            if (err){
                done(err)
            } else {
                expect(result.id).to.not.exist()
                expect(result.token).to.not.exist()
                expect(result.founded).to.equal(false)
                done()
            }    
        }
    )    
})


test('signUp failed ', (done) =>
{
    authManager.signUp
    (
        {
            username : 'unit@test.it',
            password: '1234'
        },(err,result) => 
            {
            if (err)
            {
                done(err)
            } else {
                expect(result.created).to.equal(false)
                done()
            }    
        }
    )   
})


test('signUp Ok ', (done) =>
{
    authManager.signUp
    (
        {
            username : 'newuser@unit.it',
            password: '1234',
            firstName : 'matteo',
            surname : 'bonifazi'
        },
        (err,result) => 
        {
            if (err){
                done(err)
            } else {
                expect(result.created).to.equal(true)
                expect(result._id).to.exist()
                done()
            }    
        }
    )    
})

test('validateToken Ok ', (done) =>
{
    
    authManager.signIn
    (
        {
            username : 'unit@test.it',
            password: '123'
        },
        (err,result) => 
        {
            if (err){
                done(err)
            } else {
                console.log(result.token)
                authManager.validateToken
                (
                    {
                        token : result.token,
                    },
                    (err,result) => 
                    {
                        if (err){
                            done(err)
                        } else {
                            expect(result.valid).to.equal(true)
                            expect(result.token).to.exist()
                            expect(result.id).to.exist()
                            done()
                        }    
                    }
                )
            }    
        }
    ) 
        
})

test('validateToken  failed', (done) =>
{
    authManager.validateToken
    (
        {
            token : 'tyuytyu',
        },
        (err,result) => 
        {
            if (err){
                done(err)
            } else {
                expect(result.valid).to.equal(false)
               done()
            }    
        }
    )    
})